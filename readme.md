# Void Linux installation

## About

I am exploring Void linux and this repository's shell scripts automate void installation tasks.

Installation details:

* Create ESP, boot, swap, and encrypted root partitions
* boot partition mounts to /boot
* ESP mounts to /boot/efi
* BTRFS for boot and root
* root subvolumes @, @home
* Subvolume @ mounts to /
* Subvolume @home mounts to /home
* No snapshot subvolumes - will be created using snapper, later
* Generate fstab
* Configure dracut
* Set timezone to America/Los_Angeles
* Set locale to en_US.UTF-8
* Prompt and set hostname
* Update /etc/hosts
* Prompt for a user account
* Set zsh shell for user and root
* Set passwords for user and root
* Configure refind boot manager

Intermediate features while developing vXd:

* fluxbox
* dmenu (void package not mine)

## Status

As of July 27, 2024, commit 2ee1877a works.

## To do

* packages for wireless support
* build xbps packages for void X dusk (vXd)
