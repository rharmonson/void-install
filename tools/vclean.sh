#!/bin/bash
read -p "What is the install target name? :" INSTDEV
read -p "NVME storage device? [y/n]: " yn
case $yn in
    [Yy]*) PPREFIX="p" ; break ;;  
    [Nn]*) PPREFIX="" ; break ;;
esac
umount -R /mnt
cryptsetup luksClose luks-root
cryptsetup erase "/dev/${INSTDEV}${PPREFIX}4"
swapoff "/dev/${INSTDEV}${PPREFIX}3"
sgdisk -Z "/dev/$INSTDEV"
