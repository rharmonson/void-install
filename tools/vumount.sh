#!/usr/bin/bash
read -p "What is the mount target name? :" INSTDEV
read -p "NVME storage device? [y/n]: " yn
case $yn in
    [Yy]*) PPREFIX="p" ; break ;;  
    [Nn]*) PPREFIX="" ; break ;;
esac
umount -R "/dev/$INSTDEV"
cryptsetup luksClose "/dev/${INSTDEV}${PPREFIX}4" luks-root
printf "\n"
lsblk
