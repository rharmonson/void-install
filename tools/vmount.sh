#!/usr/bin/bash
read -p "What is the mount target name? :" INSTDEV
read -p "NVME storage device? [y/n]: " yn
case $yn in
    [Yy]*) PPREFIX="p" ; break ;;  
    [Nn]*) PPREFIX="" ; break ;;
esac
$BTRFSOPTS="defaults,noatime,compress=zstd,discard=asynch"
cryptsetup luksOpen /dev/vda4 luks-root
cryptsetup luksopen "/dev/${INSTDEV}${PPREFIX}4" luks-root
#--- mount @
mount -o $BTRFSOPTS,subvol=/@ /dev/mapper/luks-root /mnt
#--- mount everything else
mount -o $BTRFSOPTS "/dev/${INSTDEV}${PPREFIX}2" /mnt/boot
mount -o rw,noatime "/dev/${INSTDEV}${PPREFIX}3" /mnt/boot/efi
mount -o $BTRFSOPTS,subvol=/@home /dev/mapper/luks-root /mnt/home
printf "\n"
lsblk
