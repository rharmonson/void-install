#!/bin/bash
#===
# chroot-install.sh
#---
# This is called by void-install.sh for xchroot installation tasks.
#===

#=== hostname
read -p 'Hostname?: ' VOIDNAME
echo "$VOIDNAME" > /etc/hostname

#=== hosts
sed -i "/^::1/a 127.0.1.1\t\$VOIDNAME.localdomain\$VOIDNAME" /etc/hosts

#=== locale
sed -i 's/^#en_US.UTF-8/en_US.utf-8/g' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

#=== timezone
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

#=== void mirror
#  Undecided if I want to include this, so comment-out.
#xmirror

# From void install manual. Write to file system for review then delete this
cp /proc/mounts /root/fstab

#=== grub2 boot loader
#  Goal is to use refind not grub. This stanza if for testing and as a
#  reference.
#  If grub-install errors on missing efi info, mount efivars.
#mount -t efivarfs none /sys/firmware/efi/efivars
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id="Void"

#=== rEFInd boot manager
# wip

#=== Reconfigure all packages
xbps-reconfigure -af

#=== sudo
printf "\nGrant wheel group sudo in sudoers\n\n"
EDITOR=vi visudo
#=== User account
printf "Create user account.\n"
read -p 'User name: ' USERNAME
useradd -s /usr/bin/bash -G wheel $USERNAME
printf "Change user password.\n\n"
passwd $USERNAME

#=== root account
printf "Change root shell and password.\n\n"
chsh -s /bin/bash root
passwd root
