# xfce4 configuration

## xfce4 configuration

system-wide: `/etc/xdg/xfce4/`

user: `~/.config/xfce4/`

## autostart

Place *.desktop file into autostart directory `/etc/xdg/autostart/`.

? how to set restart parameters as shown in the session gui app?

? in adding dwm by referencing binar in /usr/bin/, does it create a dwm.desktop file or waht?

## xfconf-query

```
xfconf-query -l (--list)
```

Results,

```
Channels:
  displays
  keyboards
  thunar
  xfce4-appfinder
  xfce4-desktop
  xfce4-keyboard-shortcuts
  xfce4-notifyd
  xfce4-panel
  xfce4-power-manager
  xfce4-screensaver
  xfce4-session
  xfce4-settings-editor
  xfce4-terminal
  xfwm4
  xsettings
```

Use with a channel.

```
xfconf-query -c xfce4-session -l
```

## watch-xfce-xfconf

https://github.com/jamescherti/watch-xfce-xfconf
