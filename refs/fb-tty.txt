voidX - fb-tty - Fri Aug  2 10:21:16 AM PDT 2024


 Private  +   Shared  =  RAM used	Program

100.0 KiB +  21.5 KiB = 121.5 KiB	pause
124.0 KiB +  26.5 KiB = 150.5 KiB	runsvdir
164.0 KiB +  31.5 KiB = 195.5 KiB	uuidd
196.0 KiB +  46.5 KiB = 242.5 KiB	acpid
216.0 KiB +  42.5 KiB = 258.5 KiB	gpm
680.0 KiB +   0.5 KiB = 680.5 KiB	runit
564.0 KiB + 194.5 KiB = 758.5 KiB	dbus-daemon
720.0 KiB +  59.5 KiB = 779.5 KiB	dhcpcd
600.0 KiB + 184.5 KiB = 784.5 KiB	login
396.0 KiB + 604.5 KiB =   1.0 MiB	peek.sh
  1.0 MiB + 337.0 KiB =   1.3 MiB	vlogger (10)
  1.2 MiB + 196.5 KiB =   1.3 MiB	VBoxService
  1.3 MiB + 129.5 KiB =   1.4 MiB	udevd
  1.0 MiB + 615.5 KiB =   1.6 MiB	bash
760.0 KiB +   1.1 MiB =   1.8 MiB	sudo (2)
  1.5 MiB + 556.5 KiB =   2.0 MiB	agetty (5)
  1.6 MiB + 519.0 KiB =   2.1 MiB	runsv (18)
  3.5 MiB + 621.5 KiB =   4.1 MiB	wpa_supplicant
  3.8 MiB + 592.5 KiB =   4.4 MiB	sshd
---------------------------------
                         25.0 MiB
=================================


runit---runsvdir-+-runsv-+-dhcpcd
                 |       `-vlogger
                 |-runsv---login---bash---sudo---sudo---peek.sh---pstree
                 |-5*[runsv---agetty]
                 |-runsv-+-udevd
                 |       `-vlogger
                 |-runsv-+-VBoxService---8*[{VBoxService}]
                 |       `-vlogger
                 |-runsv-+-acpid
                 |       `-vlogger
                 |-runsv-+-dbus-daemon
                 |       `-vlogger
                 |-runsv---gpm
                 |-runsv---vlogger
                 |-runsv
                 |-runsv-+-sshd
                 |       `-vlogger
                 |-runsv-+-pause
                 |       `-vlogger
                 |-runsv-+-vlogger
                 |       `-wpa_supplicant
                 `-runsv-+-uuidd
                         `-vlogger


System:
  Kernel: 6.6.43_1 arch: x86_64 bits: 64 compiler: gcc v: 13.2.0 clocksource: kvm-clock
  Console: pty pts/0 Distro: Void Linux
Machine:
  Type: Virtualbox System: innotek GmbH product: VirtualBox v: 1.2 serial: N/A
    Chassis: Oracle Corporation type: 1 serial: N/A
  Mobo: Oracle model: VirtualBox v: 1.2 serial: N/A uuid: d3f52da5-f41a-0a42-8989-9ff78bb49bbf
    UEFI: innotek GmbH v: VirtualBox date: 12/01/2006
Logical:
  Message: No logical block device data found.
  Device-1: luks-52deb493-013d-4dd8-9d09-b9448739598f type: LUKS dm: dm-0 size: 18.79 GiB
  Components: p-1: sda4
Info:
  Memory: total: 3.98 GiB available: 3.81 GiB used: 310.3 MiB (7.9%)
  Processes: 182 Power: uptime: 18m states: freeze,mem,disk suspend: s2idle wakeups: 0
    hibernate: shutdown Init: runit v: N/A
  Packages: pm: xbps pkgs: 588 Compilers: gcc: 13.2.0 Shell: peek.sh (sudo) default: Bash
    v: 5.2.21 running-in: pty pts/0 inxi: 3.3.35
