# void and  btrfs

x

## layout summmary

```
nvme0n1     259:0    0 465.8G  0 disk 
├─nvme0n1p1 259:2    0   200M  0 part ; fat32, label: ESP, mount: /boot/efi
└─nvme0n1p2 259:3    0 465.6G  0 part ; btrfs, label: void
        @ --> /
        @home --> /home
        @snapshots --> /.snapshots
nvme1n1     259:1    0   1.9T  0 disk 
└─nvme1n1p1 259:5    0   1.9T  0 part ; ext4, label: data, mount: /data
```

## preparation

Wipe disks.

```
wipefs -af /dev/nvme0n1
wipefs -af /dev/nvme1n1
```

To remove prior btrfs signatures from partitions, use '-t' argument, e.g., `wipefs -t btrfs /dev/nvme01n1p2`.

Use efibootmgr to clean up efi boot entries. Using 'efibootmgr' lists boot entries. Assuming entry 0000 is Windows 10, to delete is specify its boot entry number, '-b 0000' and '-B' to delete.

```
efibootmgr
efibootmgr -b 0000 -B
```

* -b, select based on boot number
* -a, set active
* -A, set inactive
* -B, delete
* -c, create
* -D, remove duplicates
* -L, set label

## partitions

```
cfdisk /dev/nvme0n1
```

1. accept gpt
2. create 200M, type EFI System
3. create new partition with all the remaining space, accept default type
4. write

```
cfdisk /dev/nvme1n1
```

Create a new partition with all space and accept default type.

## file systems

```
mkfs.fat -F32 -n ESP /dev/nvme0n1p1
```

results

```
mkfs.fat 4.2 (2021-01-31)
```

```
mkfs.btrfs -L void /dev/nvme0n1p2 <-- this will change with luks & what btrfs args if any?
```

results

```
btrfs-progs v6.5.1
See https://btrfs.readthedocs.io for more information.

Performing full device TRIM /dev/nvme0n1p2 (465.57GiB) ...
NOTE: several default settings have changed in version 5.15, please make sure
      this does not affect your deployments:
      - DUP for metadata (-m dup)
      - enabled no-holes (-O no-holes)
      - enabled free-space-tree (-R free-space-tree)

Label:              void
UUID:               fe266cec-c0cf-4b98-8207-bbb7c26fd8d5
Node size:          16384
Sector size:        4096
Filesystem size:    465.57GiB
Block group profiles:
  Data:             single            8.00MiB
  Metadata:         DUP               1.00GiB
  System:           DUP               8.00MiB
SSD detected:       yes
Zoned device:       no
Incompat features:  extref, skinny-metadata, no-holes, free-space-tree
Runtime features:   free-space-tree
Checksum:           crc32c
Number of devices:  1
Devices:
   ID        SIZE  PATH
    1   465.57GiB  /dev/nvme0n1p2
```

If an error results due to existing btrfs signature, use '-f' (force).

The data partition is for games and media not confidential information, thus ext4 not btrfs. Snapshots are not needed or desired. 

```
mkfs.ext4 -L data /dev/nvme1n1p1
```

results

```
Creating filesystem with 500099328 4k blocks and 125026304 inodes
Filesystem UUID: 6179ea69-6ce9-4dbf-8766-34b7df7119f1
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
	4096000, 7962624, 11239424, 20480000, 23887872, 71663616, 78675968, 
	102400000, 214990848

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (262144 blocks): done
Writing superblocks and filesystem accounting information: done       
```

## btrfs

Set variable BO to reduce typing.

```
BO="defaults,noatime,compress=zstd" <-- research relatime vs noatime, compression
```

Create subvolumes.

```
mount -o $BO /dev/nvme0n1p2 /mnt
btrfs subvolume create /mnt/@
btrfs su cr /mnt/@home
btrfs su cr /mnt/@snapshots
umount /mnt
```

Mount filesystems in preparation for void installation.

```
mount -o $BO,subvol=@ /dev/nvme0n1p2 /mnt <-- this will change with luks
mkdir -p /mnt/{boot/efi,home,.snapshots,data,var/cache} -- with efi not boot/efi in guide?
mount -o $BO,subvol=@home /dev/nvme0n1p2 /mnt/home <-- luks
mount -o $BO,subvol=@snapshots /dev/nvme0n1p2 /mnt/.snapshots <-- luks
mount -o rw,noatime /dev/nvme0n1p1 /mnt/boot/efi <-- again, why does the guide use efi not boot/efi?
```

results

```
# tree /mnt
mnt
├── boot
│   └── efi
├── data
├── home
└── var
    └── cache

7 directories, 0 files
```

and

```
# mount |grep mnt
/dev/nvme0n1p2 on /mnt type btrfs (rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=256,subvol=/@)
/dev/nvme0n1p2 on /mnt/home type btrfs (rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=257,subvol=/@home)
/dev/nvme0n1p2 on /mnt/.snapshots type btrfs (rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=258,subvol=/@snapshots)
/dev/nvme0n1p1 on /mnt/boot/efi type vfat (rw,noatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro)
/dev/nvme1n1p1 on /mnt/data type ext4 (rw,noatime)
```

Create subvolumes to exclude from prior subvolume snapshots. We do not want to snapshot these due to temporary data.

```
btrfs su cr /mnt/var/cache/xbps
btrfs su cr /mnt/var/tmp
btrfs su cr /mnt/srv
```

results

```
# btrfs filesystem show /mnt
Label: 'void'  uuid: b1885612-8e1d-4512-b20b-f779ca040f81
	Total devices 1 FS bytes used 240.00KiB
	devid    1 size 465.57GiB used 2.02GiB path /dev/nvme0n1p2
```

and

```
# btrfs subvolume list /mnt
ID 256 gen 11 top level 5 path @
ID 257 gen 7 top level 5 path @home
ID 258 gen 8 top level 5 path @snapshots
ID 259 gen 11 top level 256 path var/cache/xbps
ID 260 gen 11 top level 256 path var/tmp
ID 261 gen 11 top level 256 path srv
```

## void

Copy void keys.

```
mkdir -p /mnt/var/db/xbps/keys
cp -r /var/db/xbps/keys /mnt/var/db/xbps/
```

Install initial void packages.

```
xbps-install -Sy -R https://mirror.vofr.net/voidlinux/current -r /mnt \
base-system linux-mainline linux-mainline-headers btrfs-progs neovim curl grub-x86_64-efi
```

Other guide has us create pseudo-filesystem for chroot. xchroot does this for us already?
Other guide copies /etc/resolv.conf to /mnt/etc/.
Other guide has us initiate chroot with $BO and bash.

chroot using xchroot.

```
xchroot /mnt
```

locale and timezone.

```
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
sed -i 's/^#en_US.UTF-8/en_US.utf-8/g' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales
```

set hostname.

```
echo wsqy > /etc/hostname
```

update /etc/hosts to include 127.0.1.1.

```
sed '/^::1/a 127.0.1.1\t\twsqy.localdomain\twsqy' /etc/hosts
```

create /etc/fstab

```
cd /tmp
curl -LO https://raw.githubusercontent.com/rharmonson/cemkeylan-genfstab/master/genfstab
chmod +x genfstab
./genfstab -U > tmpfstab
sed -i '/^# <file/r tmpfstab' /etc/fstab
```

results

```
# cat /etc/fstab
#
# See fstab(5).
#
# <file system>	<dir>	<type>	<options>		<dump>	<pass>
# /dev/nvme0n1p2
UUID=b1885612-8e1d-4512-b20b-f779ca040f81 /               btrfs           rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=256,subvol=/@ 0 1

# /dev/nvme0n1p2
UUID=b1885612-8e1d-4512-b20b-f779ca040f81 /home           btrfs           rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=257,subvol=/@home 0 2

# /dev/nvme0n1p2
UUID=b1885612-8e1d-4512-b20b-f779ca040f81 /.snapshots     btrfs           rw,noatime,compress=zstd:3,ssd,discard=async,space_cache=v2,subvolid=258,subvol=/@snapshots 0 2

# /dev/nvme0n1p1
UUID=18AB-13C1          /boot/efi       vfat            rw,noatime,fmask=0022,dmask=0022,codepage=437,iocharset=iso8859-1,shortname=mixed,utf8,errors=remount-ro 0 2

# /dev/nvme1n1p1
UUID=6179ea69-6ce9-4dbf-8766-34b7df7119f1 /data           ext4            rw,noatime      0 2

tmpfs		/tmp	tmpfs	defaults,nosuid,nodev   0       0
```

account management.

```
passwd root
chsh -s /bin/bash root <-- why? defaults to what?
useradd techore
passwd techore
usermod -aG wheel techore
```

Permit members of wheel to sudo by uncommenting '%wheel ALL=(ALL:ALL) ALL'.

```
EDITOR=nvim visudo
```

## software

Install additional software and firmware.

```
xbps-install dhcpcd <-- already installed?
```

Ensure to identify network interface requirements including firmware; wpa_supplicant, iwd, NetworkManager, ConnMan, etc.

## services

Links services.

Verify network interface using `ip link`.


```
ln -s /etc/sv/dhcpcd-eth0 /var/service/
```

## bootloader

```
grub-install /dev/nvme0n1
```

other guide: grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id="void" <-- none of these args should be needed if using the default installation location and such. Right?


## finish

```
xbps-reconfigure -fa
exit
umount -R /mnt
reboot
```

## done?

Did it work? If not, boot from live media, xchroot, and repair.
