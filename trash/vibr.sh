#!/usr/bin/bash
#===
# void installation with btrfs and refind.
#---
# Install packages to obtain install script then clone https://gitlab.com/rharmonson/void-install.
# xbps-install -Sy git openssl neovim
#
# Use 'script' to capture output for debugging.
#===

#=== About
printf "\nvibr.sh is used to install Void Linux.

* Single disk installation
* ESP: UEFI System Partition
* BTRFS: boot and root partitions
* BTRFS subvolumes
* Swap
* rEFInd boot manager

Before proceeding ensure the following:

* Working network connection to install dependencies
* Correct time
* Disable existing swap partitions for target using 'swapoff'\n\n"

while true; do
    read -p "Continue? [y/n]: " yn
    case $yn in
        [Yy]*) break ;;  
        [Nn]*) printf "\n====>  Aborted!\n\n" ; exit ;;
    esac
done

#=== Dependencies
xbps-install -Sy gptfdisk refind xmirror

#=== Install device
#--- Print available devices
printf "Available devices:\n\n"
lsblk

#--- Select install device
printf "\nWhat device is the target for installation?
For example, sda or nvme0n1.\n"
read -p 'Device: ' INSTDEV
INSTDEV="/dev/$INSTDEV"
printf "\n====>  Installation target is $INSTDEV.\n\n"

#=== Swap size
printf "What size (GB) for the swap parition?
2 to 4 GB is sufficient for desktops, however, for
laptops and hibernate, use 100 to 110% of RAM.\n\n"

read -p 'Swap size: ' SWAPSIZE
printf "\n====>  Swap size will be $SWAPSIZE GB.\n\n"

#=== Results

if [[ $INSTDEV =~ ^nvme ]]
then
	EFIDEV="${INSTDEV}p1"
	BOOTDEV="${INSTDEV}p2"
	SWAPDEV="${INSTDEV}p3"
	ROOTDEV="${INSTDEV}p4"
else
	EFIDEV="${INSTDEV}1"
	BOOTDEV="${INSTDEV}2"
	SWAPDEV="${INSTDEV}3"
	ROOTDEV="${INSTDEV}4"
fi

printf "********** Results **********\n"
printf "  Device: $INSTDEV\n"
printf "    ESP:  $EFIDEV and 200 MB\n"
printf "    boot: $BOOTDEV and 2 GB\n"
printf "    swap: $SWAPDEV and $SWAPSIZE GB\n"
printf "    root: $ROOTDEV and all remaining space\n"

#=== Warning!

printf "\nAll data will be deleted for target device!\n"

while true; do
    read -p "Commit changes [y/n]: " yn
    case $yn in
        [Yy]*) printf "Process tasks.\n\n" ; break ;;  
        [Nn]*) printf "\n====>  Aborted!\n\n" ; exit ;;
    esac
done

#=== Create gpt table and partitions
sgdisk -Z "$INSTDEV"
sgdisk -og "$INSTDEV"
sgdisk -n 1:0:+200M -c 1:"ESP" -t 1:ef00 "$INSTDEV"
sgdisk -n 2:0:+2G -c 2:"boot" -t 2:8300 "$INSTDEV"
sgdisk -n 3:0:"+${SWAPSIZE}G" -c 3:"swap" -t 3:8200 "$INSTDEV"
sgdisk -n 4:0:0 -c 4:"void" -t 4:8304 "$INSTDEV"
sgdisk -p "$INSTDEV"
printf "\n"

#=== Create file systems
#--- efi system partition
mkfs.fat -F32 -n esp $EFIDEV
#--- btrfs volumes
mkfs.btrfs -f -L boot $BOOTDEV
mkfs.btrfs -f -L root $ROOTDEV
#--- swap
mkswap -L swap $SWAPDEV
swapon $SWAPDEV

#=== btrfs subvolumes
BTRFSOPTS='noatime,compress=zstd,discard=async'
#--- root subvolumes
mount -o $BTRFSOPTS $ROOTDEV /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
umount -R /mnt
#--- mount @
mount -o $BTRFSOPTS,subvol=/@ $ROOTDEV /mnt
#--- create mount points and mount
mkdir /mnt/boot
mount -o $BTRFSOPTS $BOOTDEV /mnt/boot
mkdir /mnt/boot/efi
mount -o rw,noatime $EFIDEV /mnt/boot/efi
mkdir /mnt/home
mount -o $BTRFSOPTS,subvol=/@home $ROOTDEV /mnt/home
#--- exclude subvolumes
#  subvolumes for the purpose of excluding ephemeral files from @ snapshots
mkdir -p /mnt/var/cache
btrfs subvolume create /mnt/var/cache/xbps
btrfs subvolume create /mnt/var/tmp
btrfs subvolume create /mnt/srv
#--- results
printf "\n"
lsblk

#=== Install void
#--- void gpg keys
mkdir -p /mnt/var/db/xbps
cp -r /var/db/xbps/keys /mnt/var/db/xbps/
#--- Install base system
xbps-install -Sy -R https://mirror.vofr.net/voidlinux/current -r /mnt base-system gptfdisk btrfs-progs refind neovim mkpasswd

#--- hostname
read -p 'Hostname?: ' VIHOSTNAME
echo $VIHOSTNAME > /mnt/etc/hostname
#--- update hosts
sed -i "/^::1/a 127.0.1.1\t\$VIHOSTNAME.localdomain\$VIHOSTNAME" /mnt/etc/hosts
#--- locale
sed -i 's/^#en_US.UTF-8/en_US.utf-8/g' /mnt/etc/default/libc-locales

# Get UUID
EFIID=$(blkid -s UUID -o value $EFIDEV)
BOOTID=$(blkid -s UUID -o value $BOOTDEV)
SWAPID=$(blkid -s UUID -o value $SWAPDEV)
ROOTID=$(blkid -s UUID -o value $ROOTDEV)
# Get BTRFS options
# | rev | cut -d_ -f2- | rev
VFATOPTS=$(mount |grep vfat |head -1 |cut -d'(' -f 2 |cut -d',' -f 1-10 |tr -d ')')
BTRFSOPTS=$(mount |grep btrfs |head -1 |cut -d'(' -f 2 |cut -d',' -f 1-5)
SWAPOPTS="defaults,noatime"

#=== Print vars to debug
#echo "EFIDEV=$EFIDEV"
#echo "BOOTDEV=$BOOTDEV"
#echo "SWAPDEV=$SWAPDEV"
#echo "ROOTDEV=$ROOTDEV"
#echo "EFIID=$EFIID"
#echo "BOOTID=$BOOTID"
#echo "SWAPID=$SWAPID"
#echo "ROOTID=$ROOTID"
#echo "VFATOPTS=$VFATOPTS"
#echo "BTRFSOPTS=$BTRFSOPTS"
#echo "SWAPOPTS=$SWAPOPTS"
 
#=== Print fstab
tee /mnt/etc/fstab <<EOF
UUID=$ROOTID    /    btrfs    $BTRFSOPTS,subvol=/@    0 1
UUID=$ROOTID    /home    btrfs    $BTRFSOPTS,subvol=/@home    0 2
UUID=$BOOTID    /boot    btrfs    $BTRFSOPTS    0 2
UUID=$EFIID    /boot/efi    vfat    $VFATOPTS    0 2
UUID=$SWAPID    swap    swap    $SWAPOPTS    0 0
tmpfs    /tmp    tmpfs    defaults,nosuid,nodev    0 0
EOF

#=== Boot manager
refind-install --root /mnt
# Above works well but it is incomplete for btrfs. Need to specify
# subvol (and perhaps other root options?).
sed -i "s/$ROOTID/$ROOTID rootflags=subvol=@/" /mnt/boot/refind_linux.conf

#=== Copy ichr.sh
cp -r /root/void-install/ichr.sh /mnt/root/

#=== chroot
xchroot /mnt /root/ichr.sh

#=== Prepare for reboot?
while true; do
    read -p "Prepare for reboot? [y/n]: " yn
    case $yn in
        [Yy]*) printf "\n====>  Devices unmounted.\n\n" ; umount -R /mnt ; break ;;  
        [Nn]*) printf "\n====>  Devices mount!\n\n" ; break ;;
    esac
done

printf "\n====>  Done!\n\n"
