#!/bin/bash
# sulogin is for single mode login, thus not linked to default.
#ln -s /etc/sv/   /etc/runit/runsvdir/default/
ln -s /etc/sv/acpid   /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus   /etc/runit/runsvdir/default/
ln -s /etc/sv/gpm   /etc/runit/runsvdir/default/
ln -s /etc/sv/iptables   /etc/runit/runsvdir/default/
ln -s /etc/sv/smartd   /etc/runit/runsvdir/default/
ln -s /etc/sv/sshd   /etc/runit/runsvdir/default/
ln -s /etc/sv/ufw   /etc/runit/runsvdir/default/
ln -s /etc/sv/uuidd   /etc/runit/runsvdir/default/
ln -s /etc/sv/wpa_supplicant   /etc/runit/runsvdir/default/
