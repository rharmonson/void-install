#!/bin/bash
#===
# Install in chroot
#---
# This is called from the void installation script using xchroot.
#===

#=== timezone
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
#=== void mirror
#xmirror
#=== xbps
xbps-reconfigure -af
#=== Account management
#--- visudo
EDITOR=vi visudo
#--- User
printf "Create user account.\n"
read -p 'User name: ' USERNAME
useradd -s /usr/bin/bash -G wheel $USERNAME
printf "Change user password.\n\n"
passwd $USERNAME
#--- root
printf "Change root password.\n\n"
chsh -s /bin/bash root
passwd root
