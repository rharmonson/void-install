#!/usr/bin/bash
#===
# void installation via chroot
# https://docs.voidlinux.org/installation/guides/chroot.html
#---
# Install packages to obtain install script then clone https://gitlab.com/rharmonson/void-install.
# xbps-install -Sy git openssl neovim
# 
# Clone repository.
# git clone https://gitlab.com/rharmonson/void-install
#
# Use 'script' to capture output for debugging.
# If using void base versus xfce, install tmux and ncurses for panes and page up/down.
#===

#=== About
printf "\nvoid-install.sh is used to install Void Linux.

* Single disk installation
* LUKS: encrypted root partition
* ESP: UEFI System Partition
* BTRFS: boot and root partitions
* BTRFS subvolumes
* Swap
* rEFInd boot manager

Before proceeding ensure the following:

* Working network connection to install dependencies
* Correct time
* Disable existing swap partitions for target using 'swapoff'\n\n"

function confirm() {
    while true; do
	printf "\n"
        read -p "Continue installation? [y/n]: " yn
        case $yn in
            [Yy]*) break ;;  
            [Nn]*) printf "\nVoid installation aborted!\n\n" ; exit ;;
        esac
    done
}

function printvars() {
    echo "EFIDEV=$EFIDEV"         # EFI System partion (ESP); /boot/efi
    echo "BOOTDEV=$BOOTDEV"       # boot partion; /boot
    echo "SWAPDEV=$SWAPDEV"       # swap partition
    echo "CRYPTDEV=$CRYPTDEV"     # root (encrypted) partition
    echo "ROOTDEV=$ROOTDEV"       # unencrypted root (/dev/mapper/luks-uuid)
    echo "EFIID=$EFIID"           # ESP UUID
    echo "BOOTID=$BOOTID"         # boot UUID
    echo "SWAPID=$SWAPID"         # swap UUID
    echo "CRYPTID=$CRYPTID"       # root (encrypted) UUID
    echo "ROOTID=$ROOTID"         # unencrypted root UUID
    echo "VFATOPTS=$VFATOPTS"     # FAT mount options
    echo "BTRFSOPTS=$BTRFSOPTS"   # BTRFS mount options
    echo "SWAPOPTS=$SWAPOPTS"     # swap mount options
    echo "ROOTMAP=$ROOTMAP"       # luks map to root (luks-uuid)
} 

confirm

#=== Dependencies
xbps-install -Sy gptfdisk refind

#=== Install device
#--- Print available devices
printf "Available devices:\n\n"
lsblk

#--- Select install device
printf "\nWhat device is the target for installation?
For example, sda or nvme0n1.\n"
read -p 'Device: ' INSTDEV
INSTDEV="/dev/$INSTDEV"
printf "\n====>  Installation target is $INSTDEV.\n\n"

#=== Swap size
printf "What size (GB) for the swap parition?
2 to 4 GB is sufficient for desktops, however, for
laptops and hibernate, use 100 to 110% of RAM.\n\n"

read -p 'Swap size: ' SWAPSIZE
printf "\n====>  Swap size will be $SWAPSIZE GB.\n\n"

#=== Results

if [[ $INSTDEV =~ "nvme" ]]
then
	EFIDEV="${INSTDEV}p1"
	BOOTDEV="${INSTDEV}p2"
	SWAPDEV="${INSTDEV}p3"
	CRYPTDEV="${INSTDEV}p4"
else
	EFIDEV="${INSTDEV}1"
	BOOTDEV="${INSTDEV}2"
	SWAPDEV="${INSTDEV}3"
	CRYPTDEV="${INSTDEV}4"
fi

printf "********** Results **********\n"
printf "  Device: $INSTDEV\n"
printf "    ESP:  $EFIDEV and 200 MB\n"
printf "    boot: $BOOTDEV and 2 GB\n"
printf "    swap: $SWAPDEV and $SWAPSIZE GB\n"
printf "    root: $CRYPTDEV and all remaining space\n"

#=== Warning!
printf "\nAll data will be deleted for target device!\n"

confirm

#=== Create gpt table and partitions
sgdisk -Z "$INSTDEV"
sgdisk -og "$INSTDEV"
sgdisk -n 1:0:+200M -c 1:"ESP" -t 1:ef00 "$INSTDEV"
sgdisk -n 2:0:+2G -c 2:"boot" -t 2:8300 "$INSTDEV"
sgdisk -n 3:0:"+${SWAPSIZE}G" -c 3:"swap" -t 3:8200 "$INSTDEV"
sgdisk -n 4:0:0 -c 4:"root" -t 4:8304 "$INSTDEV"
partx -u "$INSTDEV" ; sleep 2
sgdisk -p "$INSTDEV"
printf "\n"
lsblk
printf "\n"
blkid -s UUID "$INSTDEV"

confirm

#=== Create encrypted partition with temporary
cryptsetup luksFormat $CRYPTDEV 
CRYPTID=$(blkid -s UUID -o value $CRYPTDEV)
ROOTMAP="luks-$CRYPTID"
ROOTDEV="/dev/mapper/$ROOTMAP"
cryptsetup luksOpen $CRYPTDEV $ROOTMAP

#=== Create file systems
#--- efi system partition
mkfs.fat -F32 -n esp $EFIDEV
#--- btrfs volumes
mkfs.btrfs -f -L boot $BOOTDEV
mkfs.btrfs -f -L root $ROOTDEV
#--- swap
mkswap -L swap $SWAPDEV
swapon $SWAPDEV

#=== Get UUID
EFIID=$(blkid -s UUID -o value $EFIDEV)
BOOTID=$(blkid -s UUID -o value $BOOTDEV)
SWAPID=$(blkid -s UUID -o value $SWAPDEV)
ROOTID=$(blkid -s UUID -o value $ROOTDEV)

#=== btrfs subvolumes
BTRFSOPTS='noatime,compress=zstd,discard=async'
#--- root subvolumes
mount -o $BTRFSOPTS $ROOTDEV /mnt
btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
umount -R /mnt
#--- mount @
mount -o $BTRFSOPTS,subvol=/@ $ROOTDEV /mnt
#--- create mount points and mount
mkdir /mnt/boot
mount -o $BTRFSOPTS $BOOTDEV /mnt/boot
mkdir /mnt/boot/efi
mount -o rw,noatime $EFIDEV /mnt/boot/efi
mkdir /mnt/home
mount -o $BTRFSOPTS,subvol=/@home $ROOTDEV /mnt/home
#--- exclude subvolumes
#  subvolumes for the purpose of excluding ephemeral files from @ snapshots
mkdir -p /mnt/var/cache
btrfs subvolume create /mnt/var/cache/xbps
btrfs subvolume create /mnt/var/tmp
btrfs subvolume create /mnt/srv
#--- results
printf "\n"
lsblk
btrfs subvolume list /mnt

#=== Get file system options
#  | rev | cut -d_ -f2- | rev
VFATOPTS=$(mount |grep vfat |head -1 |cut -d'(' -f 2 |cut -d',' -f 1-10 |tr -d ')')
BTRFSOPTS=$(mount |grep btrfs |head -1 |cut -d'(' -f 2 |cut -d',' -f 1-5)
SWAPOPTS="defaults,noatime,discard"

confirm

#=== Install void
#--- void gpg keys
mkdir -p /mnt/var/db/xbps
cp -r /var/db/xbps/keys /mnt/var/db/xbps/
#--- install base system
XBPS_ARCH=x86_64 xbps-install -Sy -r /mnt -R https://mirrors.servercentral.com/voidlinux/current base-system gptfdisk cryptsetup btrfs-progs refind neovim

#=== Print fstab
tee /mnt/etc/fstab <<EOF
UUID=$ROOTID    /    btrfs    $BTRFSOPTS,subvol=/@    0 1
UUID=$ROOTID    /home    btrfs    $BTRFSOPTS,subvol=/@home    0 2
UUID=$BOOTID    /boot    btrfs    $BTRFSOPTS    0 2
UUID=$EFIID    /boot/efi    vfat    $VFATOPTS    0 2
UUID=$SWAPID    swap    swap    $SWAPOPTS    0 0
tmpfs    /tmp    tmpfs    defaults,nosuid,nodev    0 0
EOF

confirm

#== dracut
#--- dracut modules
#  'dracut --list-modules' shows modules dm, crypt, btrfs, and resume
#  already installed. So, commenting out below after testing.
tee /mnt/etc/dracut.conf.d/dracutmodules.conf <<EOF
# module support for device mapper, luks, btrfs, and resume.
# 'lsinitrd -m' shows below enabled by default but specifying them
# to ensure no surprises after an upgrade (as well as to document).
add_dracutmodules+=" dm crypt btrfs resume "
EOF
#---kernel cmdline
tee /mnt/etc/dracut.conf.d/kernelcmdline.conf <<EOF
# kernel cmdline to support encrypted root, btrfs, trim, and resume.
kernel_cmdline="ro rd.auto rd.luks.name=$CRYPTID=$ROOTMAP rd.luks.allow-discards rootfstype=btrfs rootflags=subvol=@ resume=UUID=$SWAPID" 
EOF

#=== chroot
cp chroot-install.sh package.list /mnt/root/
xchroot /mnt /root/chroot-install.sh

#=== Boot manager
#  Below works for dracut is doing the heavy lifting.
refind-install --root /mnt
sed -i 's/\s\{2,\}"$/ net.ifnames=0 quiet"/' /mnt/boot/refind_linux.conf
sed -i 's/\s\{2,\}single"$/ net.ifnames=0 quiet single"/' /mnt/boot/refind_linux.conf

#=== Prepare for reboot?
while true; do
    read -p "Unmount devices in preparation to reboot? [y/n]: " yn
    case $yn in
        [Yy]*) printf "\n====>  Devices unmounted.\n\n" ; umount -R /mnt ; cryptsetup luksClose $ROOTDEV ; break ;;  
        [Nn]*) printf "\n====>  Devices mounted!\n\n" ; break ;;
    esac
done

printf "\n====>  Done!\n\n"
