#!/bin/bash
#===
# chroot-install.sh
#---
# This is called by void-install.sh for xchroot installation tasks.
#===

#=== install packages
xbps-install -S
xargs xbps-install -y < /root/package.list

#=== configure services
# sulogin is for single mode login, thus not linked to default.
# Disabling for now to power management conflicts with elogind
#ln -s /etc/sv/acpid   /etc/runit/runsvdir/default/
ln -s /etc/sv/dbus   /etc/runit/runsvdir/default/
ln -s /etc/sv/dhcpcd /etc/runit/runsvdir/default/
ln -s /etc/sv/gpm   /etc/runit/runsvdir/default/
ln -s /etc/sv/iptables   /etc/runit/runsvdir/default/
ln -s /etc/sv/elogind /etc/runit/runsvdir/default/
ln -s /etc/sv/smartd   /etc/runit/runsvdir/default/
ln -s /etc/sv/sshd   /etc/runit/runsvdir/default/
ln -s /etc/sv/ufw   /etc/runit/runsvdir/default/
ln -s /etc/sv/uuidd   /etc/runit/runsvdir/default/
ln -s /etc/sv/wpa_supplicant   /etc/runit/runsvdir/default/
# Need to automate configuration; tuigreet
#ln -s /etc/sv/greetd /etc/runit/runsvdir/default/

#=== Host name
read -p 'Hostname?: ' VOIDNAME
echo "$VOIDNAME" > /etc/hostname

#=== Hosts
sed -i "/^::1/a 127.0.1.1\t\t$VOIDNAME.localdomain\t$VOIDNAME" /etc/hosts

#=== Locale
sed -i 's/^#en_US.UTF-8/en_US.utf-8/g' /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

#=== Time zone
ln -sf /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

#=== Menu
#  Need to figure out system-wide settings. This solution sucks.
mkdir /etc/skel/.config
cp -r file/jgmenu /etc/skel/.config/

#=== Sound
mkdir -p /etc/pipewire/pipewire.conf.d
ln -s /usr/share/examples/10-wireplumber.conf /etc/pipewire/pipewire.conf.d/
ln -s /usr/share/exmaples/20-pipewire-pulse.conf /etc/pipewire/pipewire.conf.d/
cp /usr/share/applications/pipewire.desktop /etc/xdg/autostart/

#  This assumes kernel parameter net.ifnames=0 was used.
#ln -s /etc/sv/dhcpcd-eth0 /etc/runit/runsvdir/default/

# From void install manual.
# This could be useful to grep "mapper/luks-" and $INSTDEV (void-install)
# to print for operator review or log.
#cp /proc/mounts /root/fstab

#=== sudo
printf "\nGrant wheel group sudo in sudoers\n\n"
EDITOR=vi visudo

#=== User account
printf "\nCreate user account.\n"
read -p 'User name: ' USERNAME
useradd -s /usr/bin/zsh -G wheel $USERNAME
printf "Change user password.\n\n"
passwd $USERNAME

#=== root account
printf "\nChange root shell and password.\n\n"
chsh -s /bin/zsh root
passwd root

#=== Reconfigure all packages
xbps-reconfigure -af
